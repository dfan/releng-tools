`jobs`
 - triggers, buttons, cron jobs
 - jobs call pipelines
 - seed-job needs to be run to load them after editing

`pipelines`
 - the heavy lifting
 - pipelines do things directly and call scripts