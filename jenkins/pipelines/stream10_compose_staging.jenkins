#!/usr/bin/env groovy

pipeline{
    agent none
    options{
        timeout(time: 360, unit: 'MINUTES')
    }

    parameters {
        string(name: 'dry_run', defaultValue: 'True')
        string(name: 'repoclosure_status', defaultValue: '')
    }

    stages {
        stage('Finding the Stream 10 Compose ID to stage'){
            agent {label "baremetal"}
            steps {
                script {
                    composeattrs = readJSON file: '/mnt/centos/composes/stream-10/production/latest-CentOS-Stream/compose/metadata/composeinfo.json'
                    COMPOSE_ID = composeattrs['payload']['compose']['id']
                    currentBuild.displayName = "$COMPOSE_ID"
                }
            }
        }

        stage('Staging Stream 10 Compose'){
            agent {label "baremetal"}
            steps {
                sh '''
                sudo -u compose scripts/stream10_stage.sh "${dry_run}"
                '''
            }
        }

        stage('Fixing treeinfos'){
            agent {label "baremetal"}
            steps {
                sh '''
                    [ ! -z "$dry_run" ] && echo "Dry Run" || sudo -u compose scripts/fix_treeinfos /mnt/centos/staged/10-stream/
                '''
            }
        }

        stage('Stream 10 Repoclosure Check'){
            agent {label "baremetal"}
            steps {
                script {
                  def RepoclosureStatus = sh(script: 'scripts/stream10_stage_repoclosure.sh', returnStatus: true)
                  env.repoclosure_status = RepoclosureStatus
                }
            }
            post {
                always {
                    archiveArtifacts artifacts: 'repoclosure.txt', fingerprint: true
                }
            }
        }

        stage('Staging Stream 10 Compose Isos'){
            agent {label "baremetal"}
            steps {
                sh '''
                sudo -u compose scripts/stream10_stage_isos.sh "${dry_run}"
                '''
            }
        }

    }
}
